#include <stdio.h>
#include "algorithm_tests.h"


int main() {
    int executionTimes = 1000000;

    printf("main::Starting tests for %d execution times\n", executionTimes);

    execute(executionTimes);
}
