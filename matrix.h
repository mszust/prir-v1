/*
 * Contains basic matrix algebra
 */
#ifndef PRIR_V1_MATRIX_H
#define PRIR_V1_MATRIX_H

struct Matrix {
    double **content;
    int dim;
};

/*
 * Simple function for creating matrices of doubles as 2-dimensional pointer array wrapped in struct with dimension.
 */
struct Matrix createSquareMatrix(int dim);

/*
 * Simple function for freeing the memory used for the matrix.
 */
void freeMatrix(struct Matrix matrix);

/*
 * Utility function that copies matrices for memory management purposes.
 */
void copyMatrix(struct Matrix from, struct Matrix to);

/*
 * Determines whether double is close to 0 based on default (10^-6) precision.
 */
int isNearlyZero(double x);

/*
 * Determines whether double is close to 0 comparing to as many decimal points as parametrized.
 */
int isNearlyZeroTo(double x, int decimalDigits);

/*
 * Simple function for printing matrix in human-readable form.
 */
void printMatrix(struct Matrix matrix);

/*
 * Creates identity matrix (with values 1 only on its diagonal)
 */
struct Matrix createIdentityMatrix(int dim);

/*
 * Transposes given matrix and returning the transposed matrix in result.
 */
struct Matrix transposeMatrix(struct Matrix matrix);

/*
 * Returns cofactor matrix (created by deleting one line and row), for parametrized size.
 */
struct Matrix getCofactor(struct Matrix matrix, int row, int col);

/*
 * Calculates determinant for matrix of parametrized size.
 */
double calculateDeterminant(struct Matrix matrix);

/*
 * Simply checks if matrix is invertible by testing if its determinant is not 0.
 */
void testInvertible(struct Matrix matrix);

/*
 * Multiplies 2 square matrices of same size.
 */
struct Matrix multiplyMatrices(struct Matrix matrixA, struct Matrix matrixB);

/*
 * Multiplies matrix times given doubleing point number.
 */
struct Matrix multiplyMatrix(struct Matrix matrixA, double times);

/*
 * Divides matrix by given doubleing point number.
 */
struct Matrix divideMatrix(struct Matrix matrixA, double number);

/*
 * Subtracts matrices.
 */
struct Matrix subtractMatrices(struct Matrix matrixA, struct Matrix matrixB);

/*
 * Calculates trace (sum on diagonal) for a matrix.
 */
double calculateTrace(struct Matrix matrix);

/*
 * Tests if matrix contains only zeros (nearly zeros).
 */
int isZero(struct Matrix matrix);

#endif //PRIR_V1_MATRIX_H
