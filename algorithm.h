#include "matrix.h"

#ifndef PRIR_V1_ALGORITHM_H
#define PRIR_V1_ALGORITHM_H

struct Result {
    struct Matrix matrix;
    int steps;
    double meanTimeInLoop;
};

/*
 * Calculates inverse matrix.
 */
struct Result findInverseMatrix(struct Matrix matrixA);

#endif //PRIR_V1_ALGORITHM_H
