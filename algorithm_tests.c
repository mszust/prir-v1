#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include "algorithm_tests.h"
#include "algorithm.h"
#include "matrix.h"
#include "benchmark.h"

void test2x2Correctness() {
    struct Matrix matrix = createSquareMatrix(2);

    matrix.content[0][0] = 1;
    matrix.content[0][1] = 2;
    matrix.content[1][0] = 3;
    matrix.content[1][1] = 4;

    testInvertible(matrix);

    printf("test2x2Correctness::Testing matrix:\n");
    printMatrix(matrix);

    struct Matrix expected = createSquareMatrix(2);

    //known result
    expected.content[0][0] = -2;
    expected.content[0][1] = 1;
    expected.content[1][0] = 1.5;
    expected.content[1][1] = -0.5;

    printf("test2x2Correctness::Expected result:\n");
    printMatrix(expected);

    struct Matrix result = findInverseMatrix(matrix).matrix;

    printf("test2x2Correctness::Actual result:\n");
    printMatrix(result);
    for (int i = 0; i < expected.dim; i++) {
        for (int j = 0; j < expected.dim; j++) {
            if (!isNearlyZeroTo(fabs(expected.content[i][j] - result.content[i][j]), 3)) {
                printf("test2x2Correctness:: Calculations are wrong. Aborting");
                exit(1);
            }
        }
    }
    freeMatrix(result);
    freeMatrix(expected);
}

struct Result test2x2Performance() {
    struct Matrix matrix = createSquareMatrix(2);

    matrix.content[0][0] = 1;
    matrix.content[0][1] = 2;
    matrix.content[1][0] = 3;
    matrix.content[1][1] = 4;

    testInvertible(matrix);

    return findInverseMatrix(matrix);
}

void test3x3Correctness() {
    struct Matrix matrix = createSquareMatrix(3);

    matrix.content[0][0] = 1;
    matrix.content[0][1] = 2;
    matrix.content[0][2] = 2;
    matrix.content[1][0] = -3;
    matrix.content[1][1] = 4;
    matrix.content[1][2] = 4;
    matrix.content[2][0] = -5;
    matrix.content[2][1] = 0;
    matrix.content[2][2] = 2;

    testInvertible(matrix);

    printf("test3x3Correctness::Testing matrix:\n");
    printMatrix(matrix);

    struct Matrix expected = createSquareMatrix(3);

    //known result
    expected.content[0][0] = 0.4;
    expected.content[0][1] = -0.2;
    expected.content[0][2] = 0;
    expected.content[1][0] = -0.7;
    expected.content[1][1] = 0.6;
    expected.content[1][2] = -0.5;
    expected.content[2][0] = 1;
    expected.content[2][1] = -0.5;
    expected.content[2][2] = 0.5;

    printf("test3x3Correctness::Expected result:\n");
    printMatrix(expected);

    struct Matrix result = findInverseMatrix(matrix).matrix;

    printf("test3x3Correctness::Actual result:\n");
    printMatrix(result);
    for (int i = 0; i < expected.dim; i++) {
        for (int j = 0; j < expected.dim; j++) {
            if (!isNearlyZeroTo(fabs(expected.content[i][j] - result.content[i][j]), 3)) {
                printf("test3x3Correctness:: Calculations are wrong. Aborting");
                exit(1);
            }
        }
    }
    freeMatrix(result);
    freeMatrix(expected);
}

struct Result test3x3Performance() {
    struct Matrix matrix = createSquareMatrix(3);

    matrix.content[0][0] = 1;
    matrix.content[0][1] = 2;
    matrix.content[0][2] = 2;
    matrix.content[1][0] = -3;
    matrix.content[1][1] = 4;
    matrix.content[1][2] = 4;
    matrix.content[2][0] = -5;
    matrix.content[2][1] = 0;
    matrix.content[2][2] = 2;

    testInvertible(matrix);

    return findInverseMatrix(matrix);
}

void test4x4Correctness() {
    struct Matrix matrix = createSquareMatrix(4);

    //test matrix
    matrix.content[0][0] = 1;
    matrix.content[0][1] = 2;
    matrix.content[0][2] = -2;
    matrix.content[0][3] = 2;
    matrix.content[1][0] = 3;
    matrix.content[1][1] = 4;
    matrix.content[1][2] = 4;
    matrix.content[1][3] = -4;
    matrix.content[2][0] = 5;
    matrix.content[2][1] = 0;
    matrix.content[2][2] = 2;
    matrix.content[2][3] = 2;
    matrix.content[3][0] = -1;
    matrix.content[3][1] = 5;
    matrix.content[3][2] = 3;
    matrix.content[3][3] = 2;

    testInvertible(matrix);

    printf("test4x4Correctness::Testing matrix:\n");
    printMatrix(matrix);

    struct Matrix expected = createSquareMatrix(4);

    //known result
    expected.content[0][0] = 0.108;
    expected.content[0][1] = 0.66;
    expected.content[0][2] = 0.12;
    expected.content[0][3] = -0.096;
    expected.content[1][0] = 0.183;
    expected.content[1][1] = 0.084;
    expected.content[1][2] = -0.075;
    expected.content[1][3] = 0.06;
    expected.content[2][0] = -0.266;
    expected.content[2][1] = -0.024;
    expected.content[2][2] = 0.093;
    expected.content[2][3] = 0.126;
    expected.content[3][0] = -0.003;
    expected.content[3][1] = -0.141;
    expected.content[3][2] = 0.108;
    expected.content[3][3] = 0.114;

    printf("test4x4Correctness::Expected result:\n");
    printMatrix(expected);

    struct Matrix result = findInverseMatrix(matrix).matrix;

    printf("test4x4Correctness::Actual result:\n");
    printMatrix(result);
    for (int i = 0; i < expected.dim; i++) {
        for (int j = 0; j < expected.dim; j++) {
            if (!isNearlyZeroTo(fabs(expected.content[i][j] - result.content[i][j]), 3)) {
                printf("test4x4Correctness:: Calculations are wrong. Aborting");
                exit(1);
            }
        }
    }
    freeMatrix(result);
    freeMatrix(expected);
}

struct Result test4x4Performance() {
    struct Matrix matrix = createSquareMatrix(4);

    matrix.content[0][0] = 1;
    matrix.content[0][1] = 2;
    matrix.content[0][2] = -2;
    matrix.content[0][3] = 2;
    matrix.content[1][0] = 3;
    matrix.content[1][1] = 4;
    matrix.content[1][2] = 4;
    matrix.content[1][3] = -4;
    matrix.content[2][0] = 5;
    matrix.content[2][1] = 0;
    matrix.content[2][2] = 2;
    matrix.content[2][3] = 2;
    matrix.content[3][0] = -1;
    matrix.content[3][1] = 5;
    matrix.content[3][2] = 3;
    matrix.content[3][3] = 2;

    return findInverseMatrix(matrix);
}

void test5x5Correctness() {
    struct Matrix matrix = createSquareMatrix(5);

    //test matrix
    matrix.content[0][0] = 2;
    matrix.content[0][1] = 1;
    matrix.content[0][2] = 4;
    matrix.content[0][3] = 3;
    matrix.content[0][4] = 2;
    matrix.content[1][0] = 7;
    matrix.content[1][1] = 6;
    matrix.content[1][2] = -4;
    matrix.content[1][3] = -2;
    matrix.content[1][4] = 2;
    matrix.content[2][0] = 3;
    matrix.content[2][1] = 4;
    matrix.content[2][2] = 5;
    matrix.content[2][3] = 6;
    matrix.content[2][4] = 2;
    matrix.content[3][0] = -7;
    matrix.content[3][1] = 6;
    matrix.content[3][2] = 4;
    matrix.content[3][3] = 5;
    matrix.content[3][4] = 0;
    matrix.content[4][0] = -4;
    matrix.content[4][1] = 7;
    matrix.content[4][2] = 8;
    matrix.content[4][3] = 9;
    matrix.content[4][4] = 5;

    testInvertible(matrix);

    printf("test5x5Correctness::Testing matrix:\n");
    printMatrix(matrix);

    struct Matrix expected = createSquareMatrix(5);

    //known result
    expected.content[0][0] = 0.06;
    expected.content[0][1] = 0.024;
    expected.content[0][2] = 0.102;
    expected.content[0][3] = -0.015;
    expected.content[0][4] = -0.075;
    expected.content[1][0] = 0.226;
    expected.content[1][1] = 0.11;
    expected.content[1][2] = -0.089;
    expected.content[1][3] = 0.193;
    expected.content[1][4] = -0.099;
    expected.content[2][0] = 0.842;
    expected.content[2][1] = 0.042;
    expected.content[2][2] = -0.296;
    expected.content[2][3] = 0.29;
    expected.content[2][4] = -0.235;
    expected.content[3][0] = -0.86;
    expected.content[3][1] = -0.132;
    expected.content[3][2] = 0.487;
    expected.content[3][3] = -0.285;
    expected.content[3][4] = 0.202;
    expected.content[4][0] = -0.067;
    expected.content[4][1] = 0.036;
    expected.content[4][2] = -0.196;
    expected.content[4][3] = -0.233;
    expected.content[4][4] = 0.291;

    printf("test5x5Correctness::Expected result:\n");
    printMatrix(expected);

    struct Matrix result = findInverseMatrix(matrix).matrix;

    printf("test5x5Correctness::Actual result:\n");
    printMatrix(result);
    for (int i = 0; i < expected.dim; i++) {
        for (int j = 0; j < expected.dim; j++) {
            if (!isNearlyZeroTo(fabs(expected.content[i][j] - result.content[i][j]), 3)) {
                printf("test5x5Correctness:: Calculations are wrong. Aborting");
                exit(1);
            }
        }
    }
    freeMatrix(result);
    freeMatrix(expected);
}

struct Result test5x5Performance() {
    struct Matrix matrix = createSquareMatrix(5);

    matrix.content[0][0] = 2;
    matrix.content[0][1] = 1;
    matrix.content[0][2] = 4;
    matrix.content[0][3] = 3;
    matrix.content[0][4] = 2;
    matrix.content[1][0] = 7;
    matrix.content[1][1] = 6;
    matrix.content[1][2] = -4;
    matrix.content[1][3] = -2;
    matrix.content[1][4] = 2;
    matrix.content[2][0] = 3;
    matrix.content[2][1] = 4;
    matrix.content[2][2] = 5;
    matrix.content[2][3] = 6;
    matrix.content[2][4] = 2;
    matrix.content[3][0] = -7;
    matrix.content[3][1] = 6;
    matrix.content[3][2] = 4;
    matrix.content[3][3] = 5;
    matrix.content[3][4] = 0;
    matrix.content[4][0] = -4;
    matrix.content[4][1] = 7;
    matrix.content[4][2] = 8;
    matrix.content[4][3] = 9;
    matrix.content[4][4] = 5;

    struct Result result = findInverseMatrix(matrix);
    return result;
}

void executeTest(int times, int dim) {
    double timeSum = 0;

    double startTime;
    double endTime;

    //test run, to see if the value is correct
    printf("executeTest::Checking calculation corectness for %dx%d matrix...\n", dim, dim);
    switch (dim) {
        case 2:
            test2x2Correctness();
            break;
        case 3:
            test3x3Correctness();
            break;
        case 4:
            test4x4Correctness();
            break;
        case 5:
            test5x5Correctness();
            break;
        default:
            break;
    }
    double steps = 0;
    double timeInLoopSum = 0;
    struct Result result;
    printf("executeTest::Checking calculation performance for %dx%d matrix...\n", dim, dim);
    double totalStartTime = getTime();
    for (int i = 0; i < times; i++) {
        startTime = getTime();
        switch (dim) {
            case 2:
                result = test2x2Performance();
                break;
            case 3:
                result = test3x3Performance();
                break;
            case 4:
                result = test4x4Performance();
                break;
            case 5:
                result = test5x5Performance();
                break;
            default:
                break;
        }
        endTime = getTime();
        timeSum += endTime - startTime;
        steps += result.steps;
        timeInLoopSum += result.meanTimeInLoop;
    }
    double totalEndTime = getTime();

    double totalTime = totalEndTime - totalStartTime;
    double meanTime = timeSum / times;

    printf("executeTest::Reverse matrix found meanly found in %f steps\n", steps / times);
    printf("executeTest::Total execution time for matrix %dx%d: %.10f\n", dim, dim, totalTime);
    printf("executeTest::Mean execution time for matrix %dx%d: %.10f\n", dim, dim, meanTime);
    printf("executeTest::Mean time spent in calculating single approximation for matrix %dx%d: %.10f\n", dim, dim, timeInLoopSum / times);
}

void execute(int times) {
    executeTest(times, 2);
    executeTest(times, 3);
    executeTest(times, 4);
    executeTest(times, 5);
}
