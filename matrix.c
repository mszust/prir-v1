#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include <math.h>
#include "matrix.h"

static double precision = 1e-6; // how much can the number differ from 0 to be accepted as 0.

int isNearlyZero(double x) {
    return fabs(x) < precision;
}

int isNearlyZeroTo(double x, int decimalDigits) {
    return fabs(x) < 10^-decimalDigits;
}

struct Matrix createSquareMatrix(int dim) {
    struct Matrix matrix;
    matrix.dim = dim;
    matrix.content = (double **) malloc(dim * sizeof(double *));
    for (int i = 0; i < dim; i++)
        matrix.content[i] = (double *) malloc(dim * sizeof(double *));
    return matrix;
}

void freeMatrix(struct Matrix matrix) {
    for (int i = 0; i < matrix.dim; i++)
        free(matrix.content[i]);
    free(matrix.content);
}

void printMatrix(struct Matrix matrix) {
    for (int i = 0; i < matrix.dim; i++) {
        for (int j = 0; j < matrix.dim; j++) {
            if (j == 0)
                printf("|");
            printf("%.3f ", matrix.content[i][j]);
            if (j == matrix.dim - 1)
                printf("|");
        }
        printf("\n");
    }
}

struct Matrix createIdentityMatrix(int dim) {
    struct Matrix identityMatrix = createSquareMatrix(dim);

    for (int i = 0; i < identityMatrix.dim; i++) {
        for (int j = 0; j < identityMatrix.dim; j++) {
            if (i == j) {
                identityMatrix.content[i][j] = 1;
            } else {
                identityMatrix.content[i][j] = 0;
            }
        }
    }
    return identityMatrix;
}

struct Matrix transposeMatrix(struct Matrix matrix) {
    struct Matrix transposedMatrix = createSquareMatrix(matrix.dim);
    transposedMatrix.dim = matrix.dim;

    for (int i = 0; i < matrix.dim; i++) {
        for (int j = 0; j < matrix.dim; j++) {
            transposedMatrix.content[j][i] = matrix.content[i][j];
        }
    }
    return transposedMatrix;
}

struct Matrix getCofactor(struct Matrix matrix, int row, int col) {
    int i = 0;
    int j = 0;

    struct Matrix coMatrix = createSquareMatrix(matrix.dim - 1);

    for (int mRow = 0; mRow < matrix.dim; mRow++) {
        for (int mCol = 0; mCol < matrix.dim; mCol++) {
            if (mRow != row && mCol != col) {
                coMatrix.content[i][j++] = matrix.content[mRow][mCol];
            }
            if (j == matrix.dim - 1) {
                j = 0;
                i++;
            }
        }
    }
    return coMatrix;
}

double calculateDeterminant(struct Matrix matrix) {
    if (matrix.dim == 1) {
        return matrix.content[0][0];
    }

    double determinant = 0;
    int sign = 1;

    struct Matrix coMatrix;

    for (int col = 0; col < matrix.dim; col++) {
        coMatrix = getCofactor(matrix, 0, col);
        determinant += sign * matrix.content[0][col] * calculateDeterminant(coMatrix);
        sign = -sign;
        freeMatrix(coMatrix);
    }
    return determinant;
}

void testInvertible(struct Matrix matrix) {
    double determinant = calculateDeterminant(matrix);


    if (!determinant) {
        printf("testInvertible::Matrix is not invertible! Please provide different values\n");
        exit(1);
    }
}

struct Matrix multiplyMatrices(struct Matrix matrixA, struct Matrix matrixB) {
    if (matrixA.dim != matrixB.dim) {
        printf("multiplyMatrices::Error. Square matrices to multiply must be of same size!");
        exit(1);
    }
    struct Matrix result = createSquareMatrix(matrixA.dim);
    double sum = 0;
    for (int i = 0; i < matrixA.dim; i++) {
        for (int j = 0; j < matrixA.dim; j++) {
            for (int k = 0; k < matrixA.dim; k++) {
                sum = sum + (matrixA.content[i][k] * matrixB.content[k][j]);
            }
            result.content[i][j] = sum;
            sum = 0;
        }
    }
    return result;
}

struct Matrix multiplyMatrix(struct Matrix matrixA, double times) {
    struct Matrix result = createSquareMatrix(matrixA.dim);
    for (int i = 0; i < matrixA.dim; i++) {
        for (int j = 0; j < matrixA.dim; j++) {
            result.content[i][j] = matrixA.content[i][j] * times;
        }
    }
    return result;
}

struct Matrix divideMatrix(struct Matrix matrixA, double number) {
    if (number == 0) {
        printf("divideMatrix::Cannot divide by 0!");
        exit(1);
    }

    struct Matrix result = createSquareMatrix(matrixA.dim);
    for (int i = 0; i < matrixA.dim; i++) {
        for (int j = 0; j < matrixA.dim; j++) {
            result.content[i][j] = matrixA.content[i][j] / number;
        }
    }
    return result;
}

struct Matrix subtractMatrices(struct Matrix matrixA, struct Matrix matrixB) {
    if (matrixA.dim != matrixB.dim) {
        printf("subtractMatrices::Error. Square matrices to subtract must be of same size!");
        exit(1);
    }
    struct Matrix result = createSquareMatrix(matrixA.dim);
    for (int i = 0; i < matrixA.dim; i++) {
        for (int j = 0; j < matrixA.dim; j++) {
            result.content[i][j] = matrixA.content[i][j] - matrixB.content[i][j];
        }
    }
    return result;
}

double calculateTrace(struct Matrix matrix) {
    double trace = 0;
    for (int i = 0; i < matrix.dim; i++) {
        trace = trace + matrix.content[i][i];
    }
    return trace;
}

void copyMatrix(struct Matrix from, struct Matrix to) {
    for (int i = 0; i < from.dim; i++) {
        for (int j = 0; j < from.dim; j++) {
            to.content[i][j] = from.content[i][j];
        }
    }
}

int isZero(struct Matrix matrix) {
    int isZero = 1;
    for (int i = 0; i < matrix.dim; i++) {
        for (int j = 0; j < matrix.dim; j++) {
            if (!isNearlyZero(matrix.content[i][j])) {
                isZero = 0;
                break;
            }
        }
    }
    return isZero;
}
