#ifndef PRIR_V1_BENCHMARK_H
#define PRIR_V1_BENCHMARK_H
/*
 * Simple method that can be used for measuring execution times with high precision,
 * providing custom gettimeofday() replacement for Windows systems.
 */
double getTime();

#endif //PRIR_V1_BENCHMARK_H


