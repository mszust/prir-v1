#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include "algorithm.h"
#include "matrix.h"
#include "benchmark.h"

static struct Matrix identityMatrix; // declaration of identity matrix that will be used in calculations

/*
 * Calculates first expression in the algorithm (initial matrix B).
 */
struct Matrix calculateB0(struct Matrix matrix) {
    struct Matrix result;

    struct Matrix matrixT = transposeMatrix(matrix);
    struct Matrix matrixTtimesMatrix = multiplyMatrices(matrixT, matrix);

    result = divideMatrix(matrixT, calculateTrace(matrixTtimesMatrix));

    freeMatrix(matrixT);
    freeMatrix(matrixTtimesMatrix);

    return result;
}

/*
 * Calculates n-th step of the algorithm, where n > 0.
 */
void calculateNextStep(struct Matrix matrixA, struct Matrix matrixBk) {
    struct Matrix result;
    struct Matrix twiceBk = multiplyMatrix(matrixBk, 2);
    struct Matrix BkTimesA = multiplyMatrices(matrixBk, matrixA);
    struct Matrix BkTimesAtimesBk = multiplyMatrices(BkTimesA, matrixBk);


    result = subtractMatrices(twiceBk, BkTimesAtimesBk);
    copyMatrix(result, matrixBk);

    freeMatrix(twiceBk);
    freeMatrix(BkTimesA);
    freeMatrix(BkTimesAtimesBk);
    freeMatrix(result);
}

/*
 * Calculates distance, that states how much matrix B differs from being inverse to matrix A.
 */
void calculateDistance(struct Matrix distance, struct Matrix matrixA, struct Matrix matrixB) {
    struct Matrix bTimesA = multiplyMatrices(matrixB, matrixA);

    struct Matrix result = subtractMatrices(identityMatrix, bTimesA);
    freeMatrix(bTimesA);

    copyMatrix(result, distance);
    freeMatrix(result);
}

struct Result findInverseMatrix(struct Matrix matrixA) {
    struct Result result;
    identityMatrix = createIdentityMatrix(matrixA.dim);

    //single executed logic
    struct Matrix matrixAt = transposeMatrix(matrixA);
    struct Matrix matrixBk = calculateB0(matrixA);

    struct Matrix distance = createSquareMatrix(matrixA.dim);
    calculateDistance(distance, matrixA, matrixBk);

    int step = 0;
    double timeInLoopStart = getTime();
    //loop
    while (!isZero(distance)) {
        calculateNextStep(matrixA, matrixBk);
        calculateDistance(distance, matrixA, matrixBk);
        step++;
    }
    double timeInLoopEnd = getTime();

    //cleanup
    freeMatrix(distance);
    freeMatrix(identityMatrix);
    freeMatrix(matrixA);
    freeMatrix(matrixAt);

    result.matrix = matrixBk;
    result.steps = step;
    result.meanTimeInLoop = (timeInLoopEnd - timeInLoopStart) / step;
    return result;
}
